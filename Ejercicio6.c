#include <signal.h> 	//Para trabajar con señales
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> 	// Para trabajar con fork
#include <locale.h> 	//Para simbolos latinos 
#include <sys/wait.h>	// para funciones wait y waitpid

uint flag_sigterm = 0;
uint flag_sigusr1 = 0;
uint flag_sigusr2 = 0;

void handler_sigusr(int sig) //SIGUSR1 Y SIGUSR2
{
	if (sig == SIGUSR1) //Se recibió la señal SIGUSR1
	{
		flag_sigusr1 = 1;
	}

	else if(sig==SIGUSR2)//Se recibió la señal es SIGUSR2
	{
		flag_sigusr2 = 1;
	}
		
	return;
}

void handler_sigterm(int sig) // SIGTERM
{
	flag_sigterm = 1;
}


void main()
{
	pid_t pid_fk; 	 // Se almacena lo que devuelve fork
	pid_t pid_aux=0; // PID auxiliar
	pid_t pid_hijo1; // Se almacena el PID del primer hijo

	setlocale(LC_ALL, "spanish");// Simbolos españoles
	system("clear"); // Limpia pantalla

	printf("==================================\n");
	printf("\n Opciones:\n --------------------------\n  SIGUSR1: Crear hijo\n  SIGUSR2: Crear hijo y ejecutar\n  SIGTERM: Terminar procesos\n");
	printf("\n\n PID de proceso actual: %d\n",getpid());
	printf("\n==================================\n");
	
	signal(SIGUSR1,handler_sigusr);
	signal(SIGUSR2,handler_sigusr); //Instala los handlers
	signal(SIGTERM,handler_sigterm);
		

	do
	{//------------------------------------------------------------------------
	 //SI SE RECIBE LA SEÑAL SIGUSR1
	 //------------------------------------------------------------------------
		if (flag_sigusr1 == 1)
		{	
			flag_sigusr1 = 0;

			pid_fk = fork(); // padre crea proceso hijo

			if (pid_fk == 0) //lo que ejecuta el hijo
	    	{
				printf("\n -Se recibió la señal SIGUSR1 (Hijo)-\n");

			 	while (1) 
			 	{
					printf("\n  Hijo (PID %d) - PID del padre: %d", getpid(), getppid());
					sleep(5); // 5 Segundos

					if (flag_sigterm == 1)
					{
						sleep(1);//Espera 1 segundo
 	
						if (pid_aux!=0)//Si tiene hermano mayor
							kill(pid_aux,SIGTERM); // Manda señal SIGTERM al hermano mayor
		
						printf("\n  [Terminando proceso hijo (PID %d)]\n",getpid());
						exit(0); //Termina el proceso hijo actual
					}
		 		}
    		}

			else //lo que ejecuta el padre
			{
				if (pid_aux == 0)	// Si tuvo su primer hijo
					pid_hijo1 = pid_fk;// Guarda el PID del primer hijo

				pid_aux = pid_fk; // guarda el PID del ultimo hijo -- el próximo hijo que tenga el padre va a heredar pid_aux como el pid de su hno mayor			
			}	
		}
 //------------------------------------------------------------------------
 //SI SE RECIBE LA SEÑAL SIGUSR2
 //------------------------------------------------------------------------
		else if (flag_sigusr2 == 1)
		{
			flag_sigusr2 = 0;	
			pid_fk = fork(); //padre crea proceso hijo

			if (pid_fk == 0) //lo que ejecuta el proceso hijo
			{
				char prog[]={"/bin/date"};// directorio del programa date
				char *argv[]={"/bin/date","-R",NULL};

				printf("\n -Se recibió la señal SIGUSR2 (Hijo)-\n");
				printf("\n  Hijo (PID %d)\n\n",getpid());
			
				execv(prog,argv); // Ejecuta el programa
				exit(0);	  // Termina el proceso hijo
			}
			
		}

 //------------------------------------------------------------------------
 //SI SE RECIBE LA SEÑAL SIGTERM
 //------------------------------------------------------------------------
		else if (flag_sigterm == 1)
		{
			//if(pid_fk!=0)//lo que ejecuta el padre 
			//{
				int status;
				printf("\n -Se recibió la señal SIGTERM (Padre)-\n");
				kill(pid_aux,SIGTERM); // Manda señal al ultimo hijo en ser creado
				waitpid(pid_hijo1,&status,0);// Espera a que su primer hijo (el ultimo en ser cerrado) termine
				printf("\n  [Terminando proceso padre]\n");
				exit(0);      // Termina el proceso padre
			//}
	
		}

	}while(1);
}
